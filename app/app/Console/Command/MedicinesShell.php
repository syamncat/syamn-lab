<?php

//App::uses('Shell', 'Console');

App::import('Vendor', 'UltimateOAuth');

class MedicinesShell extends AppShell {
    var $uses = array('DosingLog');

    function main() {} // メソッドを無名にする場合は main を用いる

    // コンソールからの実行確認用メソッド
    function test() {
        echo "Called test() successfully.\n";
    }

    // 通知用
    function notify_check(){
        // Load configuration
        $conf = 'Lab' . DS . 'config.php';
        Configure::load($conf);
        $medicines = Configure::read('medicines.list');
        $premarin_conf = Configure::read('medicines.premarin');

        // Get prev dosed time
        $row = $this->DosingLog->find('all', array(
            'conditions' => array(
                'DosingLog.is_deleted' => 0,
                'DosingLog.medicine_id' => $premarin_conf['id'],
            ),
            'order' => 'DosingLog.dosed_time DESC',
            'limit' => 1
        ));
        $row = $row[0]['DosingLog'];

        // compare time
        $now = date('Y-m-d H:i');
        $prev = $row['dosed_time'];
        $diffmin = (strtotime($now) - strtotime($prev)) / 60;

        // TODO remove - Logging
        CakeLog::write('medicines-notify_check', $now.' - '.$prev.' diff: '.$diffmin.'min');

        // FIXME VERY BUGGY
        // TODO check is notified or not by using the database
        $intervalmin = $premarin_conf['interval_hours'] * 60;
        $tweet = null;
        if ($diffmin === ($intervalmin - 60)){
            $tweet = '@syamncat '.$medicines[$premarin_conf['id']].'の服用1時間前です';
        }
        elseif ($diffmin === $intervalmin){
            $tweet = '@syamncat '.date('H:i', strtotime($now)).' '.$medicines[$premarin_conf['id']].' の服用時間です';
        }
        elseif ($diffmin === ($intervalmin + 180)){
            $tweet = '@syamncat '.$medicines[$premarin_conf['id']].' の服用時間を3時間過ぎています';
        }

        // Tweeting / Logging
        if (!is_null($tweet)){
            $tweet .= ' syamn.cat/lab/medicines';
            $this->_tweet($tweet);
            CakeLog::write('medicines-notify_check', 'Tweeted: '.$tweet);
        }
    }

    private function _tweet($message){
        // Skip tweeting if disabled by the configuration
        if (Configure::read('global.enable_tweeting') !== true){
            return;
        }

        $uo = new UltimateOAuth(
            Configure::read('global.twitter.consumer_key'),
            Configure::read('global.twitter.consumer_secret'),
            Configure::read('global.twitter.access_token'),
            Configure::read('global.twitter.access_token_secret')
        );
        $result = $uo->post('statuses/update', array('status' => $message));
    }
}
