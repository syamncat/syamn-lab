<?php
App::uses('AppModel', 'Model');

class User extends AppModel {
    public $useTable = 'users';
    public $validate = array(
        'name' => array(
            '名前は2文字以上で設定してください' => array('rule' => array('minLength', 2)),
            '名前が20文字を越えています' => array('rule' => array('maxLength', 20)),
        ),
    );


    /**
     * 指定したTwitterIDのユーザーが存在するか返す
     * @param $id twitter id
     * @return bool true if user exists, else false
     */
    public function isExistsByTwitterID($id){
        return ($this->find('count', array(
            'conditions' => array('User.tw_id' => $id),
            'recursive' => -1,
        )) >= 1);
    }

    /**
     * TwitterIDからAuthログイン用のユーザーオブジェクトを返す
     * @param $id twitter id
     * @return mixed User object
     */
    public function getLoginObjectByTwitterID($id){
        $user = $this->find('first', array(
                'conditions' => array('User.tw_id' => $id),
                'recursive' => -1,
            ));
        if (empty($user)){
            throw new UserNotFoundException('Twitter ID '.$id. ' is not registered');
        }
        return $user['User'];
    }
}