<?php
App::uses('AppModel', 'Model');

class DosingLog extends AppModel {
    public $useTable = 'dosing_logs';
    public $validate = array(
        'email' => array(
            '有効なメールアドレスを入力してください' => array('rule' => 'email'),
            'このメールアドレスは既に使用されています' => array('rule' => 'isUnique'),
            '100文字以下のメールアドレスしか設定できません' => array('rule' => array('maxLength', 100)),
        ),
        'medicine_id' => array(
            'おくすりのIDが不正です' => array('rule' => 'naturalNumber'),
        ),
        'medicine_quantity' => array(
            '数量は正の整数を入力してください' => array('rule' => 'naturalNumber'),
            '数量は1〜99の間で入力してください' => array('rule' => array('comparison', '<=', 99)),
        ),
        'dosed_time' => array(
            '服用日時を正しく入力してください' => array('rule' => array('datetime', 'ymd'))
        )
    );
}
?>