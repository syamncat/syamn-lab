<?php echo $this->Html->script('moment-with-langs.min', array('inline' => false)); ?>
<?php echo $this->Html->script('underscore-min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery/clndr.min', array('inline' => false)); ?>

<?php echo $this->Html->script('jquery/jquery.countdown', array('inline' => false)); ?>
<?php echo $this->Html->css('jquery.countdown', array('inline' => false)); ?>
<h2>おくすりかんり <small>Medicines panel</small></h2>
<hr />

<div class="well">
    <h4>服用登録 <small>Add dosing record</small></h4>
    <hr />
    <?php echo $this->Form->create('DosingLog', array(
        'class' => 'form-horizontal', 'role' => 'form',
        'inputDefaults' => array('label' => false, 'div' => false),
    ));?>
    <div class="form-group">
        <label for="medicine" class="col-sm-2 control-label">おくすり</label>
        <div class="col-sm-5">
            <?php
            echo $this->Form->input('medicine_id', array(
                'options' => $medicines,
                'empty' => '（選択してください）',
                'default' => 1,
                'class' => 'form-control', 'id' => 'medicine', 'required'
            ));
            ?>
        </div>
        <label for="quantity" class="col-sm-1 control-label">数量</label>
        <div class="col-sm-2">
            <div class="input-group">
                <?php echo $this->Form->input('medicine_quantity', array(
                    'placeholder' => 'n', 'class' => 'form-control', 'id' => 'quantity', 'pattern' => '^[0-9]{1,2}$',
                    'maxlength' => 2, 'min' => 1, 'max' => 99, 'default' => 2, 'required'
                ));?>
                <span class="input-group-addon">錠</span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="dosed_time" class="col-sm-2 control-label">服用日時</label>
        <div class="col-sm-5">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $this->Form->checkbox('custom_time', array(
                        'id' => 'chk_custom_time', 'default' => false
                    ));?>
                </span>
                <?php echo $this->Form->input('dosed_time', array(
                    'placeholder' => 'yyyy-mm-dd hh:mm:ss', 'class' => 'form-control', 'id' => 'dosed_time', 'maxlength' => 20, 'type' => 'text', 'required',
                    'default' => date('Y/m/d H:i'),
                ));?>
            </div>
        </div>
        <div class=" col-sm-3">
            <?php echo $this->Form->hidden('action', array('value' => 'add')); ?>
            <?php echo $this->Form->submit('登録', array('class' => 'btn btn-primary btn-block'));?>
        </div>
    </div>
    <?php echo $this->Form->end();?>
</div>

<!-- Next board -->
<div class="panel panel-default">
    <div id="panel-body-next" class="panel-body">
        <h6 class="text-center">NEXT ESTIMATED COUNTDOWN</h6>
        <div id="countdown"></div>
        <div id="alert-over" class="container" style="display: none;">
            <div class="alert alert-danger col-md-6 col-md-offset-3">予定時間を過ぎています！おくすりのんで！</div>
        </div>
        <p class="text-center">
            プレマリンの次回服用予定は<code><?php echo date('n/d H:i', $next); ?></code>です
        </p>
    </div>
</div
<!-- /Next board -->

<!-- History table -->
<div class="panel panel-primary">
    <!-- Default panel contents -->
    <div class="panel-heading">おくすりろぐ</div>

    <!-- Table -->
    <table class="table table-striped">
        <thead>
            <tr>
                <td>#</td>
                <td>服用日時</td>
                <td>おくすり</td>
                <td>数量</td>
<!--                <td>服用者</td>-->
                <td>登録者</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rows as $i => $row): ?>
                <?php if($i === 0): ?>
                <tr class="info">
                    <td><?php echo $row['DosingLog']['id']; ?></td>
                    <td><?php echo date('Y/m/d H:i', strtotime($row['DosingLog']['dosed_time'])); ?> <span class="label label-info">前回</span></td>
                    <td><?php echo h($medicines[$row['DosingLog']['medicine_id']]); ?></td>
                    <td><?php echo $row['DosingLog']['medicine_quantity']; ?>錠</td>
                    <td>しゃむ<?php //echo $row['DosingLog']['creator_id']; ?></td>
                </tr>
                <?php else: ?>
                <tr>
                    <td><?php echo $row['DosingLog']['id']; ?></td>
                    <td><?php echo date('Y/m/d H:i', strtotime($row['DosingLog']['dosed_time'])); ?></td>
                    <td><?php echo h($medicines[$row['DosingLog']['medicine_id']]); ?></td>
                    <td><?php echo $row['DosingLog']['medicine_quantity']; ?>錠</td>
                    <td>しゃむ<?php //echo $row['DosingLog']['creator_id']; ?></td>
                </tr>
                <?php endif; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<!-- TODO put calender here -->
<!--<div class="clndr">-->
<!--    <div class="clndr-grid"></div>-->
<!--</div>-->

<h4 style="margin-top: 40px;">なにこれ？ <small>About</small></h4>
<hr />
<p>
    自身のおくすり管理能力の低さから、システムに管理してもらうようにしました。<br />
    服用予定時間プラスマイナス1時間くらいで飲めるようにがんばります。<br />
    服用の登録が行わていない場合に、服用1時間前に予告通知、服用時間に催促通知、3時間後に服用/登録忘れ確認通知を送信します。<br />
    <small>（もっと作り込んで需要があればサービス化して公開したい）</small>
</p>
<ul>
    <li>14/04/09 記録だけのシステムで稼働開始</li>
    <li>14/04/11 飲み忘れ防止の通知機能を実装</li>
</ul>

<script type="text/javascript">
    $(function() {
//        $('.clndr').clndr();

        /* *** Countdown *** */
        var
            ts = new Date(<?php echo $next;?> * 1000),
//            ts = (new Date()).getTime() + 4*1000,
            stopped = false;

        $('#countdown').countdown({
            timestamp: ts,
            callback: function (left, days, hours, minutes, seconds) {
                if (!stopped && left < 0){
                    stopped = true;
                    $('#countdown').hide();
                    $('#alert-over').show();
                }
            }
        });

        /* *** Timeinput form *** */
        var $input_time = $('#dosed_time'),
            prev_value = '登録時の時間を使う (チェックして変更)',
            tmp = '';
        $('#chk_custom_time').click(function(){
            check();
        });
        check();

        function check(){
            tmp = $input_time.val();
            $input_time.val(prev_value);
            prev_value = tmp;
            if ($('#chk_custom_time').is(':checked')) {
                $input_time.removeAttr('disabled');
            }else{
                $input_time.attr("disabled", "disabled");
            }
        }
    });
</script>