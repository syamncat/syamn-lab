<h2>会員登録 <small>Register</small></h2>
<hr />

<div class="well text-center">
    <h3>キャッチ画像・テキストほか</h3>
</div>

<div class="panel panel-primary">
    <!-- Default panel contents -->
    <div class="panel-heading">会員登録</div>
    <div class="panel-body">
        <?php
        echo $this->Form->create('User', array(
            'class' => 'form-horizontal',
        ));

        echo $this->Form->input('name', array(
            'label' => '氏名',
            'placeholder' => '氏名',
            'required',
            'maxlength' => 20,
        ));

        echo $this->Form->input('name_kana', array(
            'label' => 'フリガナ',
            'placeholder' => 'フリガナ',
            'required',
            'maxlength' => 40,
        ));

        echo $this->Form->input('gender', array(
            'label' => '性別',
            'type' => 'radio',
            'inline' => true,
            'options' => array(
                1 => '男性',
                2 => '女性',
//                3 => 'その他',
            ),
            'required',
        ));
        ?>
        <div class="form-group">
            <label class="col-sm-2 control-label">メールアドレス</label>
            <div class="col-sm-10">
                <p class="form-control-static"><?php echo h($regreq['email']);?></p>
            </div>
        </div>
        <?php
        echo $this->Form->input('password', array(
            'type' => 'password',
            'label' => 'パスワード',
            'placeholder' => 'パスワード',
            'required',
            'value' => '',
        ));

        echo $this->Form->input('phone', array(
            'label' => '電話番号',
            'placeholder' => '電話番号',
            'maxlength' => 20,
        ));

        echo $this->Form->input('birthday', array(
            'label' => '生年月日',
            'dateFormat' => 'YMD',
            'monthNames' => false,
            'separator' => ' / ',
            'empty' => '--',
            'class' => 'form-inline', // TODO check it again
            'maxYear' => date('Y') - 10,
            'minYear' => date('Y') - 90, // range about 10 - 90 years old
            'required',
        ));

        echo $this->Form->input('agreed', array(
            'wrap' => 'col-sm-10 col-sm-offset-2',
            'type' => 'checkbox',
            'label' => false,
            'checkboxLabel' => '会員規約 および プライバシーポリシー に同意する',
            'required',
        ));

        echo $this->Form->input('登録', array(
            'wrap' => 'col-sm-10 col-sm-offset-2',
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'label' => false
        ));

        echo $this->Form->end();
        ?>
    </div>
</div>