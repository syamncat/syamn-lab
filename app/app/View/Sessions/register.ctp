<h2>ユーザー登録 <small>Register</small></h2>
<hr />

<div class="panel panel-primary">
    <!-- Default panel contents -->
    <div class="panel-heading">ユーザー登録</div>
    <div class="panel-body">
        <?php
        echo $this->Form->create('User', array(
            'class' => 'form-horizontal',
        ));

        echo $this->Form->input('name', array(
            'label' => 'ニックネーム(表示名)',
            'placeholder' => 'ニックネーム',
            'required',
            'maxlength' => 20,
            'default' => $screen_name,
        ));

//        echo $this->Form->input('agreed', array(
//            'wrap' => 'col-sm-10 col-sm-offset-2',
//            'type' => 'checkbox',
//            'label' => false,
//            'checkboxLabel' => '会員規約 および プライバシーポリシー に同意する',
//            'required',
//        ));

        echo $this->Form->input('ユーザー登録', array(
            'wrap' => 'col-sm-10 col-sm-offset-2',
            'type' => 'submit',
            'class' => 'btn btn-primary',
            'label' => false,
        ));

        echo $this->Form->end();
        ?>
    </div>
</div>