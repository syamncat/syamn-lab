<?php
$headLink = array(
    array('name' =>'Home', 'link' => $this->Html->link('Home', array('controller' => 'home', 'action' => 'index'))),
    array('name' => 'Medicines', 'link' => $this->Html->link('おくすり', array('controller' => 'medicines', 'action' => 'index'))),
    array('name' =>'Revisions', 'link' => $this->Html->link('リビジョン', array('controller' => 'revisions', 'action' => 'index'))),
    array('name' =>'Admin', 'link' => '<a><s>管理</s></a>'),
);
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title_for_layout . ' - しゃむらぼ'; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    // Meta
    echo $this->fetch('meta');
    echo $this->Html->meta('icon'); // favicon

    // CSS
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('main');
    echo $this->fetch('css');
    //    echo $this->Html->css('bootstrap-responsive.min'); // Need to load after the original css

    // Script
    echo $this->Html->script('jquery/jquery-1.11.0.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('main.js');
    echo $this->fetch('script');
    ?>
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
<!--            <a class="navbar-brand" href="/">しゃむらぼ</a>-->
            <?php echo $this->Html->link('しゃむらぼ', array('controller' => 'home', 'action' => 'index'), array('class' => 'navbar-brand')); ?>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php
                foreach ($headLink as $data){
                    if ($data['name'] === $this->name){
                        echo '<li class="active">'.$data['link'].'</li>';
                    }else{
                        echo '<li>'.$data['link'].'</li>';
                    }
                }
                ?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container content">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
</div><!--/.container -->

<!-- Footer -->
<div id="footer">
    <div class="container">
        <hr />
        <div class="col-md-6">
            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja" data-related="syamncat" data-dnt="true">ツイート</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        </div>
        <div class="col-md-6">
            <p class="text-muted text-right"> - <a href="https://twitter.com/syamncat" target="_blank">@syamncat</a></p>
        </div>
    </div>
</div>
<!-- /Footer -->
<?=$this->element('googleAnalytics');?>
</body>
</html>