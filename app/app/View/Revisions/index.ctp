<h2>リビジョン <small> <?php echo $repoName; ?></small></h2>
<hr />

<ul class="nav nav-tabs">
    <?php
    foreach ($repoList as $urlName => $data) {
        $link = $this->Html->link($data[1], array('controller' => 'revisions', 'action' => $urlName));
        if ($urlName == $repo) {
            echo '<li class="active">' . $link . '</li>';
        } else {
            echo '<li>' . $link . '</li>';
        }
    }
    ?>
</ul>
<table class="table table-bordered" style="margin-top: 10px;">
    <thead>
    <tr>
        <th class="col-md-1">#</th>
        <th class="col-md-1">SHA</th>
        <th class="col-md-2">日時</th>
        <th class="col-md-2">更新者</th>
        <th>コミットメッセージ</th>
    </tr>
    </thead>
    <tbody>
    <?php if (!empty($rows)): ?>
        <?php foreach ($rows as $row): ?>
            <tr>
                <td><?php echo $firstRowNo; ?></td>
                <td><?php echo h(mb_substr($row['CommitLog']['hash'], 0, 6)); ?></td>
                <td>
                    <a href="#" data-toggle="tooltip" title="<?php echo date("y/m/d H:i:s", strtotime($row['CommitLog']['date'])); ?>">
                        <?php echo $this->Common->getRelativeTime(strtotime($row['CommitLog']['date'])); ?>
                    </a>
                </td>
                <td><?php echo h($row['CommitLog']['author']); ?></td>
                <td><?php echo h($row['CommitLog']['message']); ?></td>
            </tr>
        <?php $firstRowNo++; endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="5">
                <p class="text-center"><?php echo $this->Html->link('表示するデータがありません', array('controller' => 'revisions', 'action' => $repo)); ?></p>
            </td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
<div class="text-center">
    <ul class="pagination">
        <?php
        for ($i = $page - 5, $max = $page + 5; $i <= $max; $i++) {
            if ($i <= 0) continue;
            if ($i > $lastPage) continue;

            if ($i == $page) {
                echo '<li class="active"><a>' . $i . '</a></li>';
            } else {
                $link = $this->Html->link($i, array('controller' => 'revisions', 'action' => $repo, $i));
                echo '<li>' . $link . '</li>';
            }
        }
        ?>
    </ul>
</div>
<script type="text/javascript">
    $('a[data-toggle=tooltip]').tooltip({'placement': 'bottom'}).click(function(e) {
        e.preventDefault();
    });
</script>