<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');

App::import('Vendor', 'UltimateOAuth');

class MedicinesController extends AppController {
    var $uses = array('DosingLog');
    var $medicines;

    public function beforeFilter(){
        parent::beforeFilter();

        // Load configuration
        $conf = 'Lab' . DS . 'config.php';
        Configure::load($conf);

        // Load medicines list
        $this->medicines = Configure::read('medicines.list');
        $this->set(array('medicines' => $this->medicines));
    }

    /*
     * indexアクション
     */
    public function index() {
        // Check request is post
        if ($this->request->is('post') && isset($this->data['DosingLog']['action'])){
            switch($this->data['DosingLog']['action']){
                case 'add':
                    $this->_add();
                    break;
                default:
                    $this->Session->setFlash('不正なリクエストです', 'error');
                    $this->redirect(array('action' => 'index'));
                    return;
            }
        }

        // Grab history
        $rows = $this->DosingLog->find('all', array(
            'conditions' => array('DosingLog.is_deleted' => 0),
            'order' => 'DosingLog.dosed_time DESC',
        ));

        // Get next dosing time (premarin)
        $next = $this->_get_next_time();

        // Set variables
        $this->set('title_for_layout', 'おくすり');
        $this->set(array(
            'rows' => $rows,
            'next' => $next,
        ));
    }

    /*
     * add: 履歴追加アクション
     */
    public function _add() {
        // Check client IP
        if (!in_array($this->request->clientIp(), Configure::read('medicines.allowed_ips'))){
            $this->Session->setFlash('登録できませんでした: 許可されていないIPアドレス('.$this->request->clientIp().')です', 'error');
            $this->redirect(array('controller' => 'medicines', 'action' => 'index'));
            exit;
        }

        // validaton then insert
        $this->DosingLog->create();
        $this->DosingLog->set($this->data);

        // TODO change here, set temporary values
        $this->DosingLog->set('user_id', 1);
        $this->DosingLog->set('creator_id', 1);

        // Save - success
        if($this->DosingLog->save()){
            $this->Session->setFlash('データを登録しました', 'success');

            // Build tweet string
            $date = $this->data['DosingLog']['dosed_time'];
            $quantity = $this->data['DosingLog']['medicine_quantity'];
            $medicine_id = (int)$this->data['DosingLog']['medicine_id'];
            $next = $this->_get_next_time();

            $tweet = '@syamncat '.$date.' '.$this->medicines[$medicine_id].' x'.$quantity.' もぐもぐ 次は'.date('H時i分',$next).'ごろ';
            $this->_tweet($tweet);

            // Redirect
            $this->redirect(array('controller' => 'medicines', 'action' => 'index'));
            exit;
        }
        // Error on validation
        elseif (!empty($this->DosingLog->validationErrors)){
            $errors = array();
            $msg = '';
            foreach($this->DosingLog->validationErrors as $field){
                $errors = array_merge($errors, $field);
            }
            foreach($errors as $error){
                $msg .= '<li>'.$error.'</li>';
            }
            $this->Session->setFlash('データ登録時にエラーが発生しました: <ul>'.$msg.'</ul>', 'error');
        }
        // Unknown error
        else{
            $this->Session->setFlash('データ登録時に不明なエラーが発生しました', 'error');
        }
    }

    /**
     *
     * @return int 次のプレマリン服用時間
     */
    private function _get_next_time(){
        // Get prev dosed time
        $row = $this->DosingLog->find('all', array(
            'conditions' => array(
                'DosingLog.is_deleted' => 0,
                'DosingLog.medicine_id' => Configure::read('medicines.premarin.id'),
            ),
            'order' => 'DosingLog.dosed_time DESC',
            'limit' => 1
        ));
        $time = strtotime($row[0]['DosingLog']['dosed_time']);
        $nexttime = $time + (Configure::read('medicines.premarin.interval_hours') * 60 * 60);
        return $nexttime;
    }

    private function _tweet($message){
        // Skip tweeting if disabled by the configuration
        if (Configure::read('global.enable_tweeting') !== true){
            return;
        }

        $uo = new UltimateOAuth(
            Configure::read('global.twitter.consumer_key'),
            Configure::read('global.twitter.consumer_secret'),
            Configure::read('global.twitter.access_token'),
            Configure::read('global.twitter.access_token_secret')
        );
        $result = $uo->post('statuses/update', array('status' => $message));
//        var_dump($result);
//        exit('done');
    }
}