<?php
App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');

App::import('Vendor', 'UltimateOAuth');

class RevisionsController extends AppController {
    var $uses = array('CommitLog');
//    var $components = array('Common');

    var $repoList = array(
        'web' => array(18893750, 'lab.syamn.cat'),
//        'some' => array(id, 'title'),
    );

    public function beforeFilter(){
        // Call parent filter
        parent::beforeFilter();

        // Load configuration
        $conf = 'Lab' . DS . 'config.php';
        Configure::load($conf);

        // Don't check post data only github-hooks. (But SSL connection still required.)
        if ($this->action === 'github_hooks'){
//            $this->Security->unlockedActions = array('github_hooks');
        }
    }

    public function index($repo = 'web', $page = 1) {
        // Validate
        $repo = trim(strtolower($repo));
        $repoData = $this->repoList[$repo];
        if (empty($repoData)){
            throw new NotFoundException('指定されたリポジトリが見つかりません');
        }

        $page = ((int)$page > 0) ? (int)$page : 1;
        $rowsForPage = 30;

        $rows = $this->CommitLog->find('all', array(
            'conditions' => array('CommitLog.repo_id' => $repoData[0]),
            'order' => 'CommitLog.id DESC',
            'limit' => $rowsForPage,
            'page' => $page
        ));

        // get max pages
        $count = $this->CommitLog->find('count', array('conditions' => array('CommitLog.repo_id' => $repoData[0])));
        $lastPage = (int)(($count - 1) / $rowsForPage) + 1;

        // Ser variables for View
        $this->set('firstRowNo', $page * $rowsForPage - 29);
        $this->set('rows', $rows);

        $this->set('repoName', $repoData[1]);
        $this->set('repoList', $this->repoList);
        $this->set('repo', $repo);
        $this->set('page', $page);
        $this->Set('lastPage', $lastPage);

        $this->set('title_for_layout', 'リビジョン');
    }

    public function web($page = 1){
        $this->setAction('index', 'web', $page);
    }

    public function github_hooks() {
        $this->_log('test');
        $this->autoRender = false;

//        // Load Libraries
//        App::import('Vendor', 'OAuth'.DS.'OAuthClient');
//        App::import('Vendor', 'OAuth'.DS.'Config');

        // Github settings
        $ALLOW_REPO_OWNER = array('syamn');
        $GITHUB_IPS = array('192.30.252.0/22');
        // End of github settings

        // begen new logfile
        $this->_log("==BEGEN REQUEST==", true);

        // check from github IP
        $valid_ip = false;
        foreach ($GITHUB_IPS as $ip){
            if (strpos($ip, '/') !== false){
                // CIDR ip address (subnet/bit)
                if ($this->_cidr_match($_SERVER['REMOTE_ADDR'], $ip)) {
                    $valid_ip = true;
                    break;
                }
            }
            // or simple ip address
            else if ($ip === $_SERVER['REMOTE_ADDR']){
                $valid_ip = true; break;
            }
        }
        if(!$valid_ip){
            $this->_log("Error: Invalid IP ".$_SERVER['REMOTE_ADDR']);
            header("HTTP/1.1 404 Not Found");
            exit;
        }

        // check req data
        if (!isset($_POST['payload'])){
            $this->_log("Error: Request error (POST['payload'])");
            exit;
        }

        $payload_raw = $_POST['payload'];
        try{
            $payload = json_decode($payload_raw); // json_decode(stripslashes($payload_raw));
        }catch(Exception $ex){
            $this->_log("Error: Occred exception while json_decode!");
            $this->_log(print_r($ex, TRUE));
            exit;
        }

        if (!is_object($payload) || !is_array($payload->commits)){
            $this->_log("Error: Empty or no commits payload!");
            exit;
        }

        $this->_log("Payload Contents: ");
        $this->_log(print_r($payload, TRUE));

        // get detail
        /* Pusher */
        $pusher_name = $payload->pusher->name;
        $pusher_email = $payload->pusher->email;

        /* Repository */
        $repo_name = $payload->repository->name;
        $repo_url = $payload->repository->url;
        $repo_owner = $payload->repository->owner->name;

        /* Etc */
        $ref = $payload->ref; // check pushed to master? if(=== 'refs/heads/master'){}
        $compare_url = $payload->compare;

        // Check repo owner
        if (!in_array($repo_owner, $ALLOW_REPO_OWNER)){
            $this->_log("Error: This repo owner '".$repo_owner."' not contains to allows list");
            exit;
        }

        $now = date('Y-m-d H:i:s');
        $repoId = $payload->repository->id;

        $this->_log("Starting foreach loop");
        foreach($payload->commits as $commit) {
            /* Commits */
            $commit_url = $commit->url;
            $commit_msg = $commit->message;
            $committer_name = $commit->author->name;
            $committer_username = $commit->author->username;
            $committer_email = $commit->author->email;
            $hash = $commit->id;
            $shortHash = mb_substr($hash, 0, 6);

            $this->_log("Updating database for commit sha ".$shortHash);

            // Update Database
            $newRaw = array('CommitLog' => array(
                'repo_id' => $repoId,
//                'hash' => $shortHash,
                'hash' => $hash,
                'author' => $committer_username,
                'message' => $commit_msg,
                'date' => $now
            ));
            $this->CommitLog->create();
            $this->CommitLog->save($newRaw);
        }

        // Update twitter status
        $this->_log("Building update message");

        $tweet = null;
        foreach ($this->repoList as $urlName => $data){
            if ($data[0] == (int)$repoId){
//                $tweet = '@syamncat '.$committer_username.' deployed '.$data[1].' https://syamn.cat/lab/revisions/'.$urlName.' (HEAD: '.$shortHash.')';
                $tweet = '@syamncat deployed https://syamn.cat/lab/revisions H*'.$shortHash;
            }
        }
        if (!empty($tweet)){
            if (mb_strlen($tweet) > 140){
                $tweet = mb_substr($tweet, 0, 138)."..";
            }
            $this->_log("Tweeting..: ".$tweet);
            $this->_tweet($tweet);
        }else{
            $this->_log("Tweet message is empty! Skipped tweeting!");
        }

        // end of logfile
        $this->_log("==END==");
    }

    private function _log($line, $newFile = false){
        if ($newFile){
            file_put_contents('log/last_github_hook.log', $line."\n");
        }else{
            file_put_contents('log/last_github_hook.log', $line."\n", FILE_APPEND);
        }
//        CakeLog::write('revisions-github_hooks', $line);
    }

    private function _cidr_match($ip, $range) {
        list ($subnet, $bits) = explode('/', $range);
        $ip = ip2long($ip);
        $subnet = ip2long($subnet);
        $mask = -1 << (32 - $bits);
        $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned
        return ($ip & $mask) == $subnet;
    }

    private function _tweet($message){
        // Skip tweeting if disabled by the configuration
        if (Configure::read('global.enable_tweeting') !== true){
            return;
        }

        $uo = new UltimateOAuth(
            Configure::read('global.twitter.consumer_key'),
            Configure::read('global.twitter.consumer_secret'),
            Configure::read('global.twitter.access_token'),
            Configure::read('global.twitter.access_token_secret')
        );
        $result = $uo->post('statuses/update', array('status' => $message));
    }
}