<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'DebugKit.Toolbar',
        'Session',
        'Security',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish',
                    'fields' => array('username' => 'email', 'password' => 'password'),
                    'userModel' => 'User',
                    'scope' => array('User.is_locked' => 0),
                ),
            ),
            'loginRedirect' => '/',
            'logoutRedirect' => '/',
            'loginAction' => array('controller' => 'sessions', 'action' => 'login'),
            'authError' => 'ログインしてください',
            'flash' => array('element' => 'error', 'key' => 'flash', 'params' => array()),
            'authorize' => array('Controller'),
        ),
    );

    public function beforeFilter(){
        // Require SSL connection if this isn't on the test environment
        $this->Security->blackHoleCallback = '_blackhole';
//        if (env('SERVER_ADDR') !== env('REMOTE_ADDR')){
//            $this->Security->requireSecure();
//        }

        // Call parent filter
        parent::beforeFilter();
        $this->Auth->allow();

        // Set login status for view
        $this->set('loggedIn', $this->Auth->loggedIn());
    }

    public function isAuthorized($user) {
        return true;
    }
}