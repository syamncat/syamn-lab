<?php
class UserNotFoundException extends CakeException {
    public function __construct($message = null, $code = 500) {
        if (empty($message)) {
            $message = 'Specified user does not exists';
        }
        parent::__construct($message, $code);
    }
}