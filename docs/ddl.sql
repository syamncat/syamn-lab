-- Sessions Controller
CREATE TABLE `users`
(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `tw_id` BIGINT UNSIGNED NOT NULL,
    `name` VARCHAR(30) NOT NULL,
    `oauth_token` VARCHAR(128) NOT NULL,
    `oauth_token_secret` VARCHAR(128) NOT NULL,
    `is_locked` TINYINT NOT NULL DEFAULT 0,
    `last_login` DATETIME NOT NULL,
    `last_ip` VARCHAR(40) NOT NULL,
    `created_ip` VARCHAR(40) NOT NULL,
    `created` DATETIME NOT NULL,
    `modified` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`tw_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- Medicines Controller
-- dosing_logs
CREATE TABLE dosing_logs
(
  id INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id INT UNSIGNED NOT NULL,
  dosed_time DATETIME NOT NULL,
  medicine_id INT UNSIGNED NOT NULL,
  medicine_quantity INT UNSIGNED NOT NULL,
  comment VARCHAR(200) DEFAULT NULL,
  is_deleted TINYINT DEFAULT 0 NOT NULL,
  creator_id INT UNSIGNED NOT NULL,
  created DATETIME NOT NULL
);


-- Revisions Controller
-- commit_logs
CREATE TABLE commit_logs
(
  id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  repo_id INT UNSIGNED NOT NULL,
  hash VARCHAR(40) NOT NULL,
  author VARCHAR(100) NOT NULL,
  message TEXT,
  date DATETIME NOT NULL,
  is_deleted TINYINT DEFAULT 0 NOT NULL,
  created DATETIME NOT NULL
);
CREATE INDEX repo_id ON commit_logs ( repo_id );